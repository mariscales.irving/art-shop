const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/api.js')
const app = express();
var config = require('config')
var puerto = config.get('App.webServer.port')
var fs = require('fs')
var morgan = require('morgan')
var path = require('path')
var formidable = require('express-formidable')

//app.use(bodyParser.urlencoded({extended: false}))
//app.use(bodyParser.json());

app.use(express.static('./public'));

app.listen(puerto, () => { 	
	console.log('Servidor levantado y escuchando por el puerto ' + puerto); 
	
}); 

// Crea un archivo en el directorio actual
var accessLogStream = fs.createWriteStream(path.join("./bitacora/", 'access.log'), { flags: 'a' })

// Asigna la instancia de Morgan a la aplicación
app.use(morgan('combined', { stream: accessLogStream }))

app.use(formidable({ keepExtensions:true }))

app.use(routes);

app.use((err, req, res, next) => {
	res.status(400).send(err.name); 
 })
