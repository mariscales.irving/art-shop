class Venta {
    constructor(){
        this._productos = [];
        this._comprador_nombre = "";
        this._comprador_direccion = "";
        this._comprador_cp = "";
        this._comprador_email = "";
        this._total = 0;

    }
    
    get Productos() { return this._productos}
    set Productos(value) {this._productos = value}
    get Comprador_nombre() { return this._comprador_nombre; }
    set Comprador_nombre(value) { this._comprador_nombre = value; }
    get Comprador_direccion() { return this._comprador_direccion; }
    set Comprador_direccion(value) { this._comprador_direccion = value; }
    get Comprador_cp() { return this._comprador_cp; }
    set Comprador_cp(value) { this._comprador_cp = value; }
    get Comprador_email() { return this._comprador_email; }
    set Comprador_email(value) { this._comprador_email = value; }
    get Total() { return this._total; }
    set Total(value) { this._total = value; }
    
}

module.exports = Venta;