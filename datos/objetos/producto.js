class Producto {
    constructor(){
        this._Id = "";
        this._nombre = "";
        this._precio = 0;
        this._existencias = 0;
        this._descripcion = "";
        this._foto = "";
    }
    
    get Id() { return this._Id; }
    set Id(value) { this._Id = value; }
    get Nombre() { return this._nombre; }
    set Nombre(value) { this._nombre = value; }
    get Precio() { return this._precio; }
    set Precio(value) { this._precio = value; }
    get Existencias() { return this._existencias; }
    set Existencias(value) { this._existencias = value; }
    get Descripcion() { return this._descripcion; }
    set Descripcion(value) { this._descripcion = value; }
    get Foto() { return this._foto; }
    set Foto(value) { this._foto = value; }
    
}

module.exports = Producto;