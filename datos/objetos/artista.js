class Artista {
    constructor(){
        this._id = "";
        this._nombre = "";
        this._email = "0";
        this._about = "0";
        this._usuario = "";
        this._contrasena = "";
    }
    
    get Id() { return this._id; }
    set Id(value) { this._id = value; }
    get Nombre() { return this._nombre; }
    set Nombre(value) { this._nombre = value; }
    get Email() { return this._email; }
    set Email(value) { this._email = value; }
    get About() { return this._about; }
    set About(value) { this._about = value; }
    get Usuario() { return this._usuario; }
    set Usuario(value) { this._usuario = value; }
    get Contrasena() { return this._contrasena; }
    set Contrasena(value) { this._contrasena = value; }
    
}

module.exports = Artista;