
const Control_datos = {};

//const datos = require('../datos/datos.js')
const producto = require('../objetos/producto.js')
const venta = require('../objetos/venta.js')
const artista = require('../objetos/artista.js')
const dbProducto = require('../datos/dbProductos.js')
const dbVenta = require('../datos/dbVentas.js')
const dbArtista = require('../datos/dbArtista.js')


function agregarProducto(nombre, precio, existencias, descripcion, foto, callback) {
  producto.Nombre = nombre;
  producto.Precio = precio;
  producto.Existencias = existencias;
  producto.Descripcion = descripcion;
  producto.Foto = foto;

  dbProducto.agregarProducto(producto, function (err, res) {
    if (err) return console.log(err.message)
    callback(null, res);
  });
}

function realizarVenta(productos, comprador_nombre, comprador_direccion, comprador_cp, comprador_email, callback) {

  venta.productos = []
  var total = 0;
  var i = 0;
  productos.forEach(function (element) {
    dbProducto.buscarProducto(element.id, function (err, res) {
      if (err) {
        callback(err, null);
      }
      else{
        total += parseFloat(res.precio)
        venta.productos.push({nombre: res.nombre, precio: res.precio, foto: res.foto}) 
        i++;
  
        if (i == productos.length) {
          venta.Comprador_nombre = comprador_nombre;
          venta.Comprador_direccion = comprador_direccion;
          venta.Comprador_cp = comprador_cp;
          venta.Comprador_email = comprador_email;
          venta.Total = total;
  
          dbVenta.realizarVenta(venta, function (err, res) {
            if (err) return console.log(err.message)
            productos.forEach(function (element) {
              dbProducto.actualizarCantidad(element.id, 0, function (err, res) {

              })
            })
            callback(null, res);
          });
        }
      }
    })
  });



}

function buscarVenta(id, callback) {
  dbVenta.buscarVenta(id, function (err, res) {
    callback(err, res);
  })
}

function buscarProducto(id, callback) {
  dbProducto.buscarProducto(id, function (err, res) {
    callback(err, res);
  })
}

function mostrarProductos(callback) {
  dbProducto.mostrarProductos(function (err, res) {
    if (err) return console.log(err.message)
    callback(null, res);
  })
}

function actualizarProducto(id, nombre, precio, existencias, descripcion, foto, callback) {
  dbProducto.actualizarProducto(id, nombre, precio, existencias, descripcion, foto, function (err, res) {
    callback(err, res);
  })
}

function actualizarArtista(nombre, email, abaut, callback) {
  artista.Id = "5bbda532bd109f3034eafe57";
  artista.Nombre = nombre;
  artista.Email = email;
  artista.About = abaut;

  dbArtista.actualizarArtista(artista, function (err, res) {
    callback(err, res);
  })
}


function borrarProducto(id, callback) {
  dbProducto.borrarProducto(id, function (err, res) {
    callback(err, res)
  })
}


Control_datos.agregarProducto = agregarProducto;
Control_datos.borrarProducto = borrarProducto;
Control_datos.actualizarArtista = actualizarArtista;
Control_datos.actualizarProducto = actualizarProducto;
Control_datos.mostrarProductos = mostrarProductos;
Control_datos.buscarVenta = buscarVenta;
Control_datos.buscarProducto = buscarProducto;
Control_datos.realizarVenta = realizarVenta;

module.exports = Control_datos;