const dbVentas = {};
var MongoClient = require('mongodb').MongoClient;
var config = require('config')
var url = config.get('MongoDB.connectionString')
var ObjectId = require('mongodb').ObjectId; 

function realizarVenta(venta, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      var myobj = 
        {
          productos: venta.productos,
          comprador_nombre: venta.Comprador_nombre,
          comprador_direccion: venta.Comprador_direccion,
          comprador_cp: venta.Comprador_cp,
          comprador_email: venta.Comprador_email,
          total: venta.Total
        };
      dbo.collection("Venta").insertOne(myobj, function(err, res) {
        if (err) throw ert;
        callback(null, res.insertedId);
        db.close();
      });
    });
  }

  

  function buscarVenta(id, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      try {
        var query = { "_id": ObjectId(id) };
        dbo.collection("Venta").find(query).toArray(function(err, result) {
        if (err) err.name = "Server error";
        if(result.length==0){
          let error = new Error();
          error.name="Venta doesn't exist"
          callback(error, null);
        }
        else{
          callback(err, result[0]);
        }
        db.close();
      });
      } catch (error) {
        error.name = "Invalid ID";
        callback(error, null);
      }
    });
  }

  dbVentas.realizarVenta = realizarVenta;
  dbVentas.buscarVenta = buscarVenta;
  module.exports = dbVentas;