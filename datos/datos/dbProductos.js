const dbProductos = {};
    var MongoClient = require('mongodb').MongoClient;
    var ObjectId = require('mongodb').ObjectId; 
    var config = require('config')
    var url = config.get('MongoDB.connectionString')
    var fs = require('fs');

function agregarProducto(producto, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      var myobj = 
        {
          nombre:producto.Nombre,
          precio:producto.Precio,
          existencias:producto.Existencias,
          descripcion:producto.Descripcion,
          foto:producto.Foto
        };
      dbo.collection("Producto").insertOne(myobj, function(err, res) {
        if (err) throw err;
        callback(null, res.insertedId);
        db.close();
      });
    });
  }

  function buscarProducto(id, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      try {
        var query = { "_id": ObjectId(id) };
        dbo.collection("Producto").find(query).toArray(function(err, result) {
        if (err) err.name = "Server error";
        if(result.length==0){
          let error = new Error();
          error.name="Product doesn't exist"
          callback(error, null);
        }
        else{
          callback(err, result[0]);
        }
        db.close();
      });
      } catch (error) {
        error.name = "Invalid ID";
        callback(error, null);
      }
    });
  }

  function mostrarProductos(callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      dbo.collection("Producto").find({}).toArray(function(err, result) {
        if (err) throw err;
        callback(null, result);
        db.close();
      });
    });
  }

  function actualizarProducto(id, nombre, precio, existencias, descripcion, foto, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var dbo = db.db("ecommerce");
      try {
        var myobj = { "_id": ObjectId(id) };

        if(!foto){
          var myobj2 = {$set:{
            "nombre":nombre,
            "precio":precio,
            "existencias":existencias,
            "descripcion":descripcion}};
        }
        else{
          var myobj2 = {$set:{
            "nombre":nombre,
            "precio":precio,
            "existencias":existencias,
            "descripcion":descripcion,
            "foto":foto}};
        }
        
        dbo.collection("Producto").updateOne(myobj, myobj2, function(err, res) {
        if (err) throw err;
        if(res.matchedCount==0){
          let error = new Error();
          error.name = "Product doesn't exist"
          callback(error, null);
        }
        else{
          callback(null, true);
        }
        db.close();
        });
      } catch (err) {
        err.name = "Invalid ID";
        callback(err, null);
      }
      
    });
  }


  function actualizarCantidad(id, existencias, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      var dbo = db.db("ecommerce");
      try {
        var myobj = { "_id": ObjectId(id) };

          var myobj2 = {$set:{
            "existencias":existencias}};
        
        dbo.collection("Producto").updateOne(myobj, myobj2, function(err, res) {
        if (err) throw err;
        if(res.matchedCount==0){
          let error = new Error();
          error.name = "Product doesn't exist"
          callback(error, null);
        }
        else{
          callback(null, true);
        }
        db.close();
        });
      } catch (err) {
        err.name = "Invalid ID";
        callback(err, null);
      }
      
    });
  }

  function borrarProducto(id, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      try {
        var myobj = { "_id": ObjectId(id) };

        
        buscarProducto(id, function(err, prod){
          if (err) {
            let error = new Error();
            error.name = "Product doesn't exist"
            callback(error, null);
          } else {
            if (prod.foto != "../icons/image.png") {
              var filePath = './public/assets/artworks/'+prod.foto; 
              fs.unlinkSync(filePath);
            }
          }

          dbo.collection("Producto").deleteOne(myobj, function(err, res) {
            if (err) throw err;
            if(res.deletedCount==0){
              let error = new Error();
              error.name = "Product doesn't exist"
              callback(error, null);
            }
            else{
              callback(null, true);
            }
            db.close();
          });
        })
      } catch (err) {
        err.name = "Invalid ID";
        callback(err, null);
      }
      
    });
  }

  dbProductos.actualizarProducto = actualizarProducto;
  dbProductos.agregarProducto = agregarProducto;
  dbProductos.borrarProducto = borrarProducto;
  dbProductos.buscarProducto = buscarProducto;
  dbProductos.mostrarProductos = mostrarProductos;
  dbProductos.actualizarCantidad = actualizarCantidad;
  module.exports = dbProductos;