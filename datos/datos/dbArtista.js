const dbArtista = {};
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId; 
var config = require('config')
var url = config.get('MongoDB.connectionString')

function actualizarArtista(artista, callback){
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("ecommerce");
      try {
        var myobj = { "_id": ObjectId(artista.Id) };
      } catch (error) {
        callback(err, null);
      }
      var myobj2 = 
        {$set:{
          nombre:artista.Nombre,
          email:artista.Email,
          about:artista.About
        }};
      dbo.collection("Artista").updateOne(myobj, myobj2, function(err, res) {
        if (err) throw err;
        callback(err, true);
        db.close();
      });
    });
  }

  dbArtista.actualizarArtista = actualizarArtista;
  module.exports = dbArtista;