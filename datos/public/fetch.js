
function peticion(link, method, headers, body, archivo, callback) {
    if (method=='GET'||method=='DELETE') { var request = new Request(link, { "method": method }); }
    else { if(!archivo){var request = new Request(link, { "method": method, "headers": headers, "body": body }); }
    else{var request = new Request(link, { "method": method, "body": archivo });}}
    fetch(request)
        .then(res => res.json())
        .then(data => {
            callback(data);
        })
}
