
let tabla = document.getElementById("tabla")
let carritoTotal = document.getElementById("carritoTotal")

function cargar(){
    tabla.innerHTML = 
    '<tr>'+
        '<th class="izq artwork_name">Obra</th>'+
        '<th class="der artwork_name">Precio</th>'+
        '<th class= "x"></th>'+
    '</tr>';
    carritoTotal.innerHTML = 'SUBTOTAL: $0';
    let obras = (eval(localStorage.getItem("obras")));
    if (obras.length != 0){
        let total = 0;
        obras.forEach(obra => {
            peticion('http://localhost:3000/producto/' + obra.id, 'GET', '', '', '', function (data) {
                if (data) {
                    let id = "'"+obra.id+"'";
                    tabla.innerHTML += 
                    '<tr>'+
                        '<td class="izq img-carrito"><img src="./assets/artworks/'+data.foto+'"><div>'+data.nombre+'</div></td>'+
                        '<td class="der">$'+data.precio+'</td>'+
                        '<td class="der artwork_name"><label onclick="quitar('+id+')">&times;</label></td>'+
                    '</tr>';
                    total += parseFloat(data.precio);
                    carritoTotal.innerHTML = 'SUBTOTAL: $'+total.toString();
                }
            })
        })
    }
}

function quitar(id) {
    let obras = (eval(localStorage.getItem("obras")));
    var pos = obras.indexOf({"id":id});
    obras.splice(pos, 1);
    localStorage.setItem("obras",JSON.stringify(obras))
    cargar()
}