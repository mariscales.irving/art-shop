
let tabla = document.getElementById("tabla")
let carritoTotal = document.getElementById("carritoTotal")
let destino = document.getElementById("destino")
let destinatario = document.getElementById("destinatario")
let email = document.getElementById("email")

function cargar() {
    tabla.innerHTML =
        '';
    carritoTotal.innerHTML = 'TOTAL: $0';
    peticion('http://localhost:3000/venta/' + sessionStorage.getItem("venta"), 'GET', '', '', '', function (data) {
        if (data) {
            let obras = data.productos;
            if (obras.length != 0) {
                let total = 0;
                obras.forEach(obra => {
                    tabla.innerHTML +=
                        '<tr>' +
                        '<td class="izq img-carrito"><img src="../assets/artworks/' + obra.foto + '"><div>' + obra.nombre + '</div></td>' +
                        '<td class="der">$' + obra.precio + '</td>' +
                        '</tr>';
                    total += parseFloat(obra.precio);
                    carritoTotal.innerHTML = 'TOTAL: $' + total.toString();
                })
            }
            destino.innerHTML = data.comprador_direccion + data.comprador_cp
            destinatario.innerHTML = data.comprador_nombre
            email.innerHTML = data.comprador_email
        }
    })
}