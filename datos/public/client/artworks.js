var request = new Request('http://localhost:3000/productos', {
    method: 'GET', 
    headers: new Headers({
        'Content-Type': 'application/json'
	})
});

var carrito = document.getElementById('carrito');
var container = document.querySelector('#container');
var cantidad = document.querySelector('#cantidad');
var floating = document.getElementById('floating-button');
var carritoTotal = document.getElementById('carrito-total');
var contenido = "";




function getProductos(){
    peticion('http://localhost:3000/productos', 'GET', '', '', '', function (artworks) {

        Vue.component('item',{
            props: ['_id','nombre','precio','existencias','foto',],
            template: `
            <div class='item'>
                <div class='image'>
                    <a :href="'http://localhost:3000/artwork?id='+_id">
                        <img :src="'./assets/artworks/'+foto">
                        <div id='vendido' :data-existencias="''+existencias">
                            <div>Vendido</div>
                        </div>
                    </a>
                </div>
                <div class='artwork_info'>
                    <div class='artwork_name'>
                        <a :href="'http://localhost:3000/artwork?id='+_id">{{ nombre }}</a>
                    </div>
                    <div class='artwork_price'>
                        <a :href="'http://localhost:3000/artwork?id='+_id">$ {{ precio }}</a>
                    </div>
                </div>
            </div>
            `
        })

        new Vue({
            el: '#container',
            data: {
                'artworks': artworks
            }
        })

        
    })
    mostrarCarrito();
}

function mostrarCarrito() {
    let obras = (eval(localStorage.getItem("obras")));
    carrito.innerHTML = obras.length;
    if (obras.length == 0) {floating.style.display = 'none'}
    else{
        floating.style.display = 'flex'
        let total = 0;
        obras.forEach(obra => {
            peticion('http://localhost:3000/producto/' + obra.id, 'GET', '', '', '', function (data) {
                if (data) {
                    total += parseFloat(data.precio);
                    carritoTotal.innerHTML = '$'+total.toString()
                }
            })
        })
    }
}