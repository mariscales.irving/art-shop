var imagen = document.querySelector('#imagen');
var descripcion = document.querySelector('#descripcion');
var cantidad = document.querySelector('#cantidad');
var nombre = document.querySelector('#nombre');
var precio = document.querySelector('#precio');
var obras = document.getElementById('carrito');
var floating = document.getElementById('floating-button');
var carritoTotal = document.getElementById('carrito-total');
var comprar = document.getElementById('comprar');



function getInfo() {
    peticion('http://localhost:3000/producto/' + window.location.search.substr(1).split('=')[1], 'GET', '', '', '', function (data) {
        if (data.existencias == 0) {
            comprar.value = 'NO DISPONIBLE'
            comprar.style.background = 'gray'
            comprar.onclick = '#'
        }
        imagen.innerHTML = "<img src='./assets/artworks/" + data.foto + "'>";
        nombre.innerHTML = data.nombre;
        descripcion.innerHTML = data.descripcion;
        precio.innerHTML = "$" + data.precio;
    })
    mostrarCarrito();
}

function mostrarCarrito() {
    let obras = (eval(localStorage.getItem("obras")));
    carrito.innerHTML = obras.length;
    if (obras.length == 0) {floating.style.display = 'none'}
    else{
        floating.style.display = 'flex'
        let total = 0;
        obras.forEach(obra => {
            peticion('http://localhost:3000/producto/' + obra.id, 'GET', '', '', '', function (data) {
                if (data) {
                    total += parseFloat(data.precio);
                    carritoTotal.innerHTML = '$'+total.toString()
                }
            })
        })
    }
}

function agregarCarrito() {
    let obra = { "id": window.location.search.substr(1).split('=')[1]}
    if (!localStorage.getItem("obras")) { //si no existe la lista de compras
        localStorage.setItem("obras", '[]');
    }
    else {
        let obras = (eval(localStorage.getItem("obras")));
        let exists = false;
        obras.forEach(element => {
            if (obra.id == element.id) { //si no está ya en la lista
                exists = true;
            }
        });
        if (!exists) {
            obras.push(obra);
            localStorage.setItem("obras", JSON.stringify(obras));
            carrito.innerHTML = (eval(localStorage.getItem("obras"))).length;
            mostrarCarrito();
        }
    }
}