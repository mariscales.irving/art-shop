
nombre = document.getElementById("nom")
apellido = document.getElementById("apellido")
direccion = document.getElementById("direccion")
ciudad = document.getElementById("ciudad")
estado = document.getElementById("estado")
pais = document.getElementById("pais")
cp = document.getElementById("cp")
email = document.getElementById("email")
form = document.getElementById("form")

function pagar() {
    let obras = (eval(localStorage.getItem("obras")));
    peticion('http://localhost:3000/venta/', 'POST', new Headers({ 'Content-Type': 'application/json' }), JSON.stringify({
        "productos": obras,
        "comprador_nombre": nombre.value+" "+ apellido.value,
        "comprador_direccion": direccion.value+". "+ciudad.value+". "+estado.value+". "+pais.value+". ",
        "cp": cp.value,
        "comprador_email": email.value
    }), '', function (data) {
        sessionStorage.setItem("venta", data.idVenta)
        window.location.href = 'http://localhost:3000/vendido/';
    })
    localStorage.setItem("obras", "[]")
}
