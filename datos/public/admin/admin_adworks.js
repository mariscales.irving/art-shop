

let info;
var contenido = "";
let seleccionado;
let modal = document.getElementById('miModal');
let modalEliminar = document.getElementById('miModal-eliminar');
let deleteTitle = document.getElementById('delete-title');
let flex = document.getElementById('flex');
let abrir = document.getElementsByClassName('abrir');
var container = document.querySelector('#opcion');
let nombre = document.getElementById('inp-name');
let precio = document.getElementById('inp-precio');
let descripcion = document.getElementById('inp-des');
let imagen = document.getElementById('image-edit');
let file = document.getElementById('file-input');
let nuevo = document.getElementById('crear-nuevo');
let submit = document.getElementById('inp-submit');
let eliminar = document.getElementById('inp-del');
let check = document.getElementById('check');
nuevo = nuevo.outerHTML;

function getProductos() {
    contenido = "";
    peticion('http://localhost:3000/productos', 'GET', '', '', '', function (data) {
        makeItem(data)
    })
}

function getInfo(id) {
    peticion('http://localhost:3000/producto/' + id, 'GET', '', '', '', function (data) {
        if (data.existencias == 1) {check.checked = true;} 
        else {check.checked = false;}
        nombre.value = data.nombre;
        precio.value = data.precio;
        descripcion.innerHTML = data.descripcion;
        descripcion.value = data.descripcion;
        imagen.src = "../assets/artworks/" + data.foto;
        deleteTitle.innerHTML = '¿Eliminar '+data.nombre+' de tu inventario?';
    })
}

function postArtwork() {
    if (!file.files.item(0)) { foto = "../icons/image.png"; }
    else {foto = file.files.item(0).name;postFile();}
    if (check.checked) {existencias = 1;} 
    else {existencias = 0;}
    peticion('http://localhost:3000/producto/', 'POST', new Headers({'Content-Type': 'application/json'}), JSON.stringify({
        "nombre": "" + nombre.value + "",
        "precio": "" + precio.value + "",
        "descripcion": "" + descripcion.value + "",
        "existencias": "" + existencias + "",
        "foto": foto
    }), '', function (data) {
        getProductos();
    })
}

function postFile() {
    const formData = new FormData()
    formData.append('archivo', file.files.item(0))
    peticion('http://localhost:3000/file/', 'POST', '', '', formData, function (data) { console.log(data)})
}

function putArtwork() {
    if (!file.files.item(0)) { foto = ""; }
    else {foto = file.files.item(0).name; postFile();}
    if (check.checked) {existencias = 1;} 
    else {existencias = 0;}
    peticion('http://localhost:3000/producto/' + seleccionado, 'PUT', new Headers({'Content-Type': 'application/json'}), JSON.stringify({
        "nombre": "" + nombre.value + "",
        "precio": "" + precio.value + "",
        "descripcion": "" + descripcion.value + "",
        "existencias": "" + existencias + "",
        "foto": foto
    }), '', function (data) {
        getProductos();
    })
}

function deleteArtwork() {
    peticion('http://localhost:3000/producto/' + seleccionado, 'DELETE', '','','', function (data) {
        getProductos();
    })
    cerrarEliminar();
    cerrarModal();
}



function validar(callback) {
    if (form.checkValidity()) {
        modal.style.display = 'none';
        callback()
    }
}

function cerrarModal(){
    modal.style.display = 'none';
}

function cerrarEliminar(){
    modalEliminar.style.display = 'none';
}

function abrirEliminar(){
    modalEliminar.style.display = 'flex';
}

function archivo(evt) {
    var files = evt.target.files;
    var reader = new FileReader();
    reader.onload = (function () {
        return function (e) {
            imagen.src = e.target.result;
        };
    })(files[0]);
    reader.readAsDataURL(files[0]);
}
document.getElementById('file-input').addEventListener('change', archivo, false);

function makeItem(data){
    data.forEach(function (element) {
        contenido +=
            "<div>" +
            "<div class='image_admin'>" +
            "<a href='#' class='abrir' data-id='" + element._id + "'>" +
            "<img src='../assets/artworks/" + element.foto + "'" +
            "alt=''>" +
            "</a>" +
            "</a>" +
            "</div>" +
            "<div class='artwork_info'>" +
            "<div class='artwork_name'>" +
            "<a href='#' class='abrir' data-id='" + element._id + "'>" + element.nombre + "</a>" +
            "</div>" +
            "</div>" +
            "</div>";
    })
    container.innerHTML = "";
    container.innerHTML = nuevo;
    container.innerHTML += contenido;

    for (let i = 0; i < abrir.length; i++) {
        abrir[i].addEventListener('click', function () {
            if (i < 2) {
                submit.value = "Crear"
                submit.onclick = function () { validar(function () { postArtwork() }) }
                eliminar.style.display = 'none';
            } else {
                submit.value = "Actualizar"
                submit.onclick = function () { validar(function () { putArtwork() }) }
                eliminar.style.display = 'inline-block';
            }
            nombre.value = "";
            precio.value = "";
            descripcion.innerHTML = "";
            descripcion.value = "";
            modal.style.display = 'block';
            imagen.src = "../assets/icons/image.png";
            if (abrir[i].dataset.id) {
                getInfo(abrir[i].dataset.id);
                seleccionado = abrir[i].dataset.id;
            }
        })
    }
}
