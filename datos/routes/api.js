const control = require('../control_datos/control_datos.js');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const jwtKey = 'my_secret_key';
var fs = require('fs');
var path    = require("path");

router.get('/vendido', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/client/vendido.html'));
});

router.get('/artworks', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/client/artworks.html'));
});

router.get('/artwork/', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/client/artwork_info.html'));
});

router.get('/admin/', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/admin/admin_artworks.html'));
});

router.get('/carrito/', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/client/carrito.html'));
});

router.get('/compra/', (req, res) => { //direccionamiento
		res.sendFile(path.join(__dirname+'/../public/client/compra.html'));
});

router.get('/productos/', (req, res) => { //direccionamiento
	control.mostrarProductos(function (err, producto, next) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		res.status(200).send(producto);
	})
});

router.get('/venta/:id', (req, res, next) => { //direccionamiento
	control.buscarVenta(req.params.id, function (err, venta) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (venta) res.status(200).send(venta);
	})
});

router.get('/producto/:id', (req, res, next) => { //direccionamiento
	control.buscarProducto(req.params.id, function (err, producto) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (producto) res.status(200).send(producto);
	})
});

router.post('/file',  (req, res, next) => {
	fs.rename(req.files.archivo.path, path.join(__dirname+"/../public/assets/artworks/"+req.files.archivo.name) , function(){
		res.status(200).send({ message: 'se agregó el producto'});
	})
});

router.post('/producto',  (req, res, next) => {
	if (Object.keys(req.fields).length === 0) {
		let error = new Error();
		error.name = 'Missing body'
		error.httpStatusCode = 400;
		return next(error)
	}
	control.agregarProducto(req.fields.nombre, req.fields.precio, req.fields.existencias, req.fields.descripcion, req.fields.foto, function (err, result) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (result) res.status(200).send({ message: 'se agregó el producto', idProducto: result });
	});
})

router.put('/producto/:id', (req, res, next) => { //direccionamiento
//console.log(req.fields.existencias)
	if (Object.keys(req.fields).length === 0) {
		let error = new Error();
		error.name = 'Missing body'
		error.httpStatusCode = 400;
		return next(error)
	}
	control.actualizarProducto(req.params.id, req.fields.nombre, req.fields.precio, req.fields.existencias, req.fields.descripcion, req.fields.foto, function (err, result) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (result) res.status(200).send({ message: 'se actualizó el producto' });
	})
});

router.delete('/producto/:id', (req, res, next) => { //direccionamiento
	control.borrarProducto(req.params.id, function (err, result) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (result) res.status(200).send({ message: 'se borró el producto' });
	})
});

router.post('/venta', (req, res, next) => {
	if (Object.keys(req.fields).length === 0) {
		let error = new Error();
		error.name = 'Missing body'
		error.httpStatusCode = 400;
		return next(error)
	}
	control.realizarVenta(req.fields.productos, req.fields.comprador_nombre, req.fields.comprador_direccion, req.fields.cp, req.fields.comprador_email, function (err, result) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (result) res.status(200).send({ message: 'se realizó la venta', idVenta: result });
	});
})

router.put('/artista', ensureToken, (req, res, next) => {
	if (Object.keys(req.body).length === 0) {
		let error = new Error();
		error.name = 'Missing body'
		error.httpStatusCode = 400;
		return next(error)
	}
	control.actualizarArtista(req.body.nombre, req.body.email, req.body.about, function (err, result) {
		if (err) {
			err.httpStatusCode = 400;
			return next(err)
		}
		if (result) res.status(200).send({ message: 'se actualizaron los datos' });
	})
})

router.post('/token', function (req, res) {
	const user = req.body;
	const token = jwt.sign({ user }, jwtKey)
	res.send(token)
})

function ensureToken(req, res, next) {
	const bearerHeader = req.headers['authorization']
	if (typeof bearerHeader !== 'undefined') {
		const bearer = bearerHeader.split(" ");
		const bearerToken = bearer[1];
		req.token = bearerToken;
		jwt.verify(req.token, jwtKey, (err, data) => {
			if (err) {
				err.name = 'Invalid access'
				err.httpStatusCode = 403;
				return next(err)
			}
		})
		next()
	}
	else {
		res.sendStatus(403)
	}
}

module.exports = router;

